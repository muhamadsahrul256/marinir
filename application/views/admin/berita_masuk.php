<!-- Main row -->
<div class="row">
  <section class="col-lg-12 connectedSortable">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No.</th>
          <th>Pengirim</th>
          <th>Judul Berita</th>
          <th>Lampiran</th>
          <th>Status</th>
          <th>Pilihan</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $key => $value): ?>
        <tr>
          <td><?=$key+1?></td>
          <td><?=$value['position']?></td>
          <td><?=$value['title_news']?></td>
          <td><a href="<?=base_url('admin/download-file/'.$value['news_id'].'/'.$value['file']);?>" target="_blank"><?=$value['file']?></a></td>
          <td><?=$value['status_in_news']?></td>
          <td>
            <a class="btn btn-primary btn-sm" href="<?= base_url('admin/view-berita-masuk/'.$value['id_berita']); ?>"><i class="fas fa-eye"></i> View</a>
            <a class="btn btn-danger btn-sm" href="<?=base_url('admin/delete-berita-masuk/'.$value['news_id']); ?>" onclick="return confirm('Yakin ingin menghapus?')"><i class="fas fa-trash"></i> Delete</a>
            <a class="btn btn-primary btn-sm" href="<?= base_url('admin/print-berita/'.$value['id_berita']); ?>"><i class='fas fa-print'></i>Print</a>
          </td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </section>
</div>
<!-- /.row (main row) -->