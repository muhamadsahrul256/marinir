<!-- Main row -->
<div class="row">
    <section class="col-lg-12 connectedSortable">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Daftar Users</h3>&nbsp;
                <a href="<?=base_url('admin/users');?>"><i class="fa fa-plus"></i></a>
            </div>
            <div class="card-body">  
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>NRP</th>
                        <th>Posisi</th>
                        <th>Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $key => $value): ?>
                        <tr>
                        <td><?=$key+1?></td>
                        <td><?=$value['name']?></td>
                        <td><?=$value['email']?></td>
                        <td><?=$value['NRP']?></td>
                        <td><?=$value['position']?></td>
                        <td>
                            <a class="btn btn-info btn-sm" href="<?=base_url('admin/edit-user/').$value['id']?>"><i class="fas fa-pencil-alt"></i> Edit</a>
                            <a class="btn btn-danger btn-sm" href="<?=base_url('admin/delete-user/').$value['id']?>" onclick="return confirm('Yakin ingin menghapus?')"><i class="fas fa-trash"></i> Delete</a>
                        </td>
                        </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
  </section>
</div>
<!-- /.row (main row) -->