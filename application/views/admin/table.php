<!-- Main row -->
<div class="row">
  <section class="col-lg-12 connectedSortable">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No.</th>
          <th>Col 1</th>
          <th>Col 2</th>
          <th>Col 3</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $key => $value): ?>
        <tr>
          <td><?=$key+1?></td>
          <td><?=$value['col_1']?></td>
          <td><?=$value['col_2']?></td>
          <td><?=$value['col_3']?></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
  </section>
</div>
<!-- /.row (main row) -->