<html>
<head>
  <title>Report Table</title>
  <style type="text/css">
    table{
      border-collapse: collapse;
      font-family: arial;
      color:#5E5B5C;
    }
 
    thead th{
      text-align: left;
      padding: 10px;
    }
 
    tbody td{
      border-top: 1px solid #e3e3e3;
      padding: 10px;
    }
 
    tbody tr:nth-child(even){
      background: #F6F5FA;
    }
 
    tbody tr:hover{
      background: #EAE9F5
    }
  </style>
</head>
<?php
$klasifikasi = 'Umum';
$lineColor = 'black';
if ($component_message->news_category == 2) {
	$klasifikasi = 'Rahasia';
	$lineColor = 'red';
}
?>
<body>
	<table width="90%">
		<tr>
			<td align="left" width="50%">TENTARA NASIONAL INDONESIA<BR>MARKAS BERSAR ANGKATAN LAUT<HR width="90%"></td>
			<td width="30%">&nbsp;</td>
			<td align="left">
				REGISTRASI
				<br>
				<br>
				No......................................
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<h2>FORMULIR BERITA</h2>
				<hr width="99%">
			</td>
		</tr>
	</table>
	<table border="1" width="100%">
		<tr>
			<th valign="top" width="40%" height="60px">
				Panggilan (Petungguk PAHUB)
				
			</th>
			<th valign="top" width="10%" height="60px">
				Jenis<br>
				<?=$component_message->news_jenis?>
			</th>
			<th valign="top" width="10%" height="60px">
				No.<br>
				<?=$component_message->news_nomor?>
			</th>
			<th valign="top" width="15%" height="60px">
				Derajat<br>
				<?=$component_message->news_derajat?>
			</th>
			<th valign="top" width="25%" height="60px">
				Instr mengirim
			</th>
		</tr>
		<tr>
			<th valign="top" rowspan="2" colspan="3" style="border-left:4px solid <?=$lineColor?>;border-top:4px solid <?=$lineColor?>;border-right:4px solid <?=$lineColor?>;">
				<BR><BR>
				DARI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?="{$component_message->position}"?><BR><BR>
				KEPADA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?=implode(', ', $receiver)?><BR><BR>
				TEMBUSAN &nbsp;: <?=implode(', ', $cc)?><BR>
			</th>
			<th valign="top" colspan="2" height="70px">Tanggal/waktu pengunjukan :<BR>
					<?=date('m.d.Y', strtotime($component_message->news_waktu_pengunjukan));?></th>
		</tr>
		<tr>
			<th valign="top" height="50px" style="border-bottom:4px solid <?=$lineColor?>;">Tanda Dinas</th>
			<th valign="top" height="50px" style="border-bottom:4px solid <?=$lineColor?>;">Grup</th>
		</tr>
		<tr>
			<td style="border-left:4px solid <?=$lineColor?>; border-top: 1px solid black; width: 10px;"></td>
			<td colspan="5" style="border-top: 1px solid black; width: 97%;">
				<div>
					<?php
						// echo nl2br(trim(substr($component_message->news_content, 0, 1525)));
						echo nl2br(trim(str_replace('<hr>', '', $component_message->news_content)));
					?>
				</div>
			</td>
		</tr>
	</table>

	<table border="1" width="100%">
		<tbody>
			<tr>
				<td colspan="3" style="border-bottom:none;border-left:4px solid <?=$lineColor?>;background: #F6F5FA;border-top:1px solid black;"><br><br>Pengirim<br></td>
				<td colspan="2" valign="top" align="center" height="10px" style="border-top:1px solid black; border-left:1px solid black;">Derajat</td>
				<td colspan="2" valign="top" align="center" height="10px" style="border-left:4px solid <?=$lineColor?>;border-top:4px solid <?=$lineColor?>;">Waktu</td>
				<td colspan="1" valign="top" align="center" height="10px" style="border-top:4px solid <?=$lineColor?>; border-left:1px solid black;">Per</td>
				<td colspan="1" valign="top" align="center" height="10px" style="border-top:4px solid <?=$lineColor?>; border-left:1px solid black;">Paraf</td>
			</tr>
			<tr>
				<td colspan="3" style="border-left:4px solid <?=$lineColor?>;">
					Nama 		:<br>
					<br>Pangkat/Jabatan  	:<br>
					<br>Tandatangan	:<br>
				</td>
				<td valign="top" align="center" height="80px">Aksi</td>
				<td valign="top" align="center" height="80px"><font style="font-size:11px; ">Tembusan</font></td>
				<td valign="top" align="center" height="80px" style="border-left:4px solid <?=$lineColor?>; border-top:1px solid black;">Terima</td>
				<td valign="top" align="center" height="80px">Kirim</td>
				<td><p></p></td>
				<td><p></p></td>
				<td><p></p></td>
				<td><p></p></td><td><p></p></td>
			</tr>
		</tbody>
	</table>
	<?php //die(); ?>
</body>
</html>