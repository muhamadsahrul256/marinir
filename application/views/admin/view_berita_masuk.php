<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                Berita Keluar
                </h3>
                <!-- tools box -->
                <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
                <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
                <form action="<?= base_url('admin/send-berita'); ?>" method="post" enctype="multipart/form-data">
                    <div class="icheck-primary d-inline">
                        <input type="radio" id="radioPrimary1" name="kategori_berita" value="1" <?= ($news->category == '1') ? 'checked' : ''; ?> disabled>
                        <label for="radioPrimary1">
                            Umum
                        </label>
                    </div>
                    <div class="icheck-danger d-inline">
                        <input type="radio" name="kategori_berita" id="radioDanger1" value="2" <?= ($news->category == '2') ? 'checked' : ''; ?> disabled>
                        <label for="radioDanger1">
                            Rahasia
                        </label>
                    </div>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="exampleInputFile">Judul Berita</label>
                        <input type="text" class="form-control" placeholder="Judul berita" name="title" value="<?=$news->title?>" readonly>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label>Penerima</label>
                        <select class="select2" multiple="multiple" data-placeholder="Penerima" style="width: 100%;" name="receiver[]" disabled>
                            <?php foreach($data_user as $k => $v): ?>
                                <?php foreach($receiver as $val): 
                                    if($val['user_id'] == $v['id']){ ?>
                                        <option value="<?= $v['id']; ?>" selected><?= $v['position']; ?></option>
                                    <?php }else{ ?>
                                        <option value="<?= $v['id']; ?>"><?= $v['position']; ?></option>
                                    <?php }
                                endforeach; 
                            endforeach;?>
                        </select>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label>Tembusan</label>
                        <select class="select2" multiple="multiple" data-placeholder="Tembusan" style="width: 100%;" name="cc[]" disabled>
                            <?php foreach($data_user as $k => $v): 
                                foreach($cc as $val):
                                    if($val['user_id'] == $v['id']){?>
                                        <option value="<?= $v['id']; ?>" selected><?= $v['position']; ?></option>
                                    <?php }else{ ?>
                                        <option value="<?= $v['id']; ?>"><?= $v['position']; ?></option>
                                    <?php }
                                endforeach;
                            endforeach; ?>
                        </select>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="exampleInputFile">Nomor</label>
                        <input type="text" class="form-control" placeholder="Nomor" name="nomor" value="<?= $news->nomor; ?>" readonly>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="exampleInputFile">Jenis</label>
                        <input type="text" class="form-control" placeholder="Jenis" name="jenis" value="<?= $news->jenis; ?>" readonly>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="exampleInputFile">Derajat</label>
                        <input type="text" class="form-control" placeholder="Derajat" name="derajat" value="<?= $news->derajat; ?>" readonly>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="exampleInputFile">Tgl/Waktu Pengunjukan</label>
                        <input type="date" class="form-control" placeholder="Tgl/Waktu Pengunjukan" name="waktu_pengunjukan" value="<?= $news->waktu_pengunjukan; ?>" readonly>
                    <div class="mt-10" style="margin-top: 10px;"></div>
                    <label for="content">Isi Berita</label>
                        <div class="mb-3">
                            <textarea name="content" id="content" class="textarea" placeholder="Place some text here"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" disabled><?= $news->content; ?></textarea>
                        </div>
                    <div class="col-md-2 button-right">
                    <a href="<?=base_url('admin/berita-masuk')?>" class="btn btn-info btn-block">Back</a>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
    <!-- /.col-->
</div>