<!-- form start -->
<form role="form" action="<?=base_url('admin/save-user');?>" method="post">
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputName">Name</label>
      <input type="text" class="form-control" id="exampleInputName" placeholder="Masukan name" name="name">
    </div>
    <div class="form-group">
      <label for="exampleInputNRP">NRP</label>
      <input type="text" class="form-control" id="exampleInputNRP" placeholder="Masukan nrp" name="nrp">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email</label>
      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Masukan email" name="email">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
    </div>
    <div class="form-group">
      <label for="exampleInputTitle">Jabatan Organik</label>
      <input type="text" class="form-control" id="exampleInputTitle" placeholder="Masukan Jabatan Organik" name="title">
    </div>
    <div class="form-group">
      <label for="exampleInputPosition">Jabatan Latihan</label>
      <input type="text" class="form-control" id="exampleInputPosition" placeholder="Masukan Jabatan Latihan" name="position">
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>