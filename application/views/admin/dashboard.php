
<img src="assets/<?=isset($setting['banner']) ? $setting['banner'] : '/images/backdrop-min.jpg'?>" style="width: 100%;" alt="">
<div class="row">
    <div class="col-md-12">
    <center>
        <h1 style="margin-top:50px; margin-bottom:50px;font-family: cursive;word-wrap: break-word;">
            <p><b><?=isset($setting['welcome_text']) ? $setting['welcome_text'] : 'TNI Maju'?><b></p>
        </h1></center>
    </div>
</div>
<?php $icons = json_decode($setting['icons']); ?>
<div class="row">
    <?php for($i=0; $i < count($icons); $i++) { ?>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="<?= $icons[$i]->icon ?>"></i></span>

            <div class="info-box-content">
                <span class="info-box-text"><?= $icons[$i]->name ?></span>
                <span class="info-box-number"><?= $icons[$i]->data ?></span>
                <a href="<?=base_url('admin/download-file/').base64_encode('assets'.$icons[$i]->file);?>" target="_blank">Download File</a>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <?php } ?>
</div>
    <!-- /.row -->