<!-- form start -->
<form role="form" action="<?=base_url('admin/update-user');?>" method="post">
  <input type="hidden" name="id" value="<?=$id?>">
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputName">Name</label>
      <input name="name" type="text" class="form-control" id="exampleInputName" value="<?=$data->name?>" placeholder="Masukan name">
    </div>
    <div class="form-group">
      <label for="exampleInputNRP">NRP</label>
      <input name="nrp" type="text" class="form-control" id="exampleInputNRP" value="<?=$data->NRP?>" placeholder="Masukan nrp">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email</label>
      <input name="email" type="email" class="form-control" id="exampleInputEmail1" value="<?=$data->email?>" placeholder="Masukan email">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password <small>*Jika tidak ingin diubah, tidak perlu di isi</small></label>
      <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
      <label for="exampleInputTitle">Jabatan Organik</label>
      <input name="title" type="text" class="form-control" id="exampleInputTitle" value="<?=$data->title?>" placeholder="Masukan Jabatan Organik">
    </div>
    <div class="form-group">
      <label for="exampleInputPosition">Jabatan Latihan</label>
      <input name="position" type="text" class="form-control" id="exampleInputPosition" value="<?=$data->position?>" placeholder="Masukan Jabatan Latihan">
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Update</button>
  </div>
</form>