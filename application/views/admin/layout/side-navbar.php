<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <!-- <li class="nav-item has-treeview menu-open"> -->
            <a href="<?= base_url('/admin'); ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dasbor
              </p>
            </a>
          </li>
      

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Berita
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('admin/berita-keluar'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Berita Keluar</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('admin/berita-masuk'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Berita Masuk</p>
                </a>
              </li>
          
            </ul>
          </li>
          <?php if($this->session->userdata('level') == 1): ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Ruang Admin
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('admin/list-users'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('admin/settings'); ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pengaturan</p>
                </a>
              </li>
             <!--  <li class="nav-item">
                <a href="pages/tables/data.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Berita Masuk</p>
                </a>
              </li> -->
          
            </ul>
          </li>
          <?php endif; ?>
          <li class="nav-item">
            <a href="<?= base_url('admin/kirim-berita'); ?>" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Kirim Berita
              </p>
            </a>
          </li>
        </ul>
      </nav>