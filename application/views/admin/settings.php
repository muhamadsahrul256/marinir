<!-- form start -->
<form role="form" action="<?=base_url('admin/save-setting');?>" method="post" enctype="multipart/form-data">
  <div class="card-body">
    <div class="form-group">
      <label for="">Spanduk</label>
      <input type="file" class="form-control file-upload" name="banner" value="<?= $data['banner'] ?>" accept="image/*">
      <br>
      <img src="<?=base_url('assets'.$data['banner'])?>" alt="" width="50%" id="preview">
    </div>
    <div class="form-group">
      <label for="">Teks Sambutan</label>
      <input type="text" class="form-control" placeholder="Enter Text" value="<?= $data['welcome_text']; ?>" name="welcome_text">
    </div>
    <span>Follow this <a href="https://fontawesome.com/cheatsheet" target="_blank">link</a> for example icon, and if you want to change the icon just change the last word</span>
    <div class="row">
        <?php for($i=0; $i < 3; $i++) { ?>
        <div class="col-md-4">
            <div class="form-group">
                <label>Icon</label>
                <input type="text" class="form-control" placeholder="Enter Icon" name="icon_icon[<?=$i+1?>]" value="<?= isset($data_icons[$i]->icon) ? $data_icons[$i]->icon : ''; ?>">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" placeholder="Enter Name" name="icon_name[<?=$i+1?>]" value="<?= isset($data_icons[$i]->name) ? $data_icons[$i]->name : ''; ?>">
            </div>
            <div class="form-group">
                <label>Data</label>
                <input type="text" class="form-control" placeholder="Enter Data" name="icon_data[<?=$i+1?>]" value="<?= isset($data_icons[$i]->data) ? $data_icons[$i]->data : ''; ?>">
            </div>
            <div class="form-group">
                <label>File</label>
                <input type="file" class="form-control" name="icon_file<?=$i+1?>" onchange="readURL(this, 'preview<?=$i?>')" accept="image/*">
                <input type="hidden" name="file_hidden[<?=$i+1?>]" value="<?= isset($data_icons[$i]->file) ? $data_icons[$i]->file : ''; ?>">
                <br>
                <img src="<?= isset($data_icons[$i]->file) ? base_url('assets').$data_icons[$i]->file : ''; ?>" alt="" width="50%" id="preview<?=$i?>">
            </div>
        </div>
        <?php } ?>
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>

<script>
function readURL(input, id) {
    if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
        $('#'+id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
    }
}

$(".file-upload").change(function() {
    readURL(this, 'preview');
});

</script>