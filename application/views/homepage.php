<!DOCTYPE html>
<html class="js sizes customelements history pointerevents postmessage webgl websockets cssanimations csscolumns csscolumns-width no-csscolumns-span csscolumns-fill csscolumns-gap csscolumns-rule csscolumns-rulecolor csscolumns-rulestyle csscolumns-rulewidth no-csscolumns-breakbefore no-csscolumns-breakafter no-csscolumns-breakinside flexbox picture srcset webworkers" lang="zxx">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Homepage | Marinir</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="https://colorlib.com/preview/theme/logistico/img/favicon.png">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>bootstrap.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>font-awesome.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>themify-icons.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>flaticon.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>animate.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>jquery-ui.css">
	<link rel="stylesheet" href="<?=base_url('assets/homepage/')?>style.css">
	<style>
	.slider_bg_homepage{
		background-image:url(assets<?=isset($setting['banner']) ? $setting['banner'] : '/images/backdrop-min.jpg'?>)
	}
	</style>
</head>
<body>
	<!--[if lte IE 9]>
	            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
	        <![endif]-->
	<div class="slider_area">
		<div class="single_slider  d-flex align-items-center slider_bg_homepage">
			<div class="container">
				<div class="row align-items-center justify-content-center">
					<div class="col-xl-12">
						<div class="slider_text text-center justify-content-center">
							<h3 style="word-wrap: break-word;"><?=isset($setting['welcome_text']) ? $setting['welcome_text'] : 'TNI Maju'?></h3>
							<a class="boxed-btn3" href="<?=base_url('login')?>">Login</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="transportaion_area">
		<div class="container">
			<div class="row text-center">
				<div class="col-xl-4 col-lg-4 col-md-6">
					<div class="single_transport">
						<div class="icon" style="margin-bottom: 20px;">
							<img src="<?=base_url('assets/homepage/')?>target-3.png" alt="" style="max-width: 20%;">
						</div>
						<h3>Sample Text</h3>
						<p>Esteem spirit temper too say adieus who direct esteem. It look estee luckily or picture
						placing drawing.</p>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6">
					<div class="single_transport">
						<div class="icon" style="margin-bottom: 20px;">
							<img src="<?=base_url('assets/homepage/')?>target-3.png" alt="" style="max-width: 20%;">
						</div>
						<h3>Sample Text</h3>
						<p>Esteem spirit temper too say adieus who direct esteem. It look estee luckily or picture
						placing drawing.</p>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6">
					<div class="single_transport">
						<div class="icon" style="margin-bottom: 20px;">
							<img src="<?=base_url('assets/homepage/')?>target-3.png" alt="" style="max-width: 20%;">
						</div>
						<h3>Sample text</h3>
						<p>Esteem spirit temper too say adieus who direct esteem. It look estee luckily or picture
						placing drawing.</p>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<script src="<?=base_url('assets/homepage/')?>jquery-1.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>bootstrap.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>ajax-form.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_002.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_003.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_004.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>plugins.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_006.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_007.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>jquery_005.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>mail-script.js" type="text/javascript"></script>
	<script src="<?=base_url('assets/homepage/')?>main.js" type="text/javascript"></script>

	<script async="" src="<?=base_url('assets/homepage/')?>js" type="text/javascript"></script>
</body>
</html>