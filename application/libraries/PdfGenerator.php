<?php

class PdfGenerator
{
  public function generate($html,$filename)
  {
    $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->AddPage();
    $pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, '');
    //Close and output PDF document
    $pdf->Output($filename.'.pdf', 'I');
  }
}