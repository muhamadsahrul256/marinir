<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class users extends CI_Model {
    private $table = 'tb_users';
    public function getUsers($email)
    {
        if($email != ''){
            $this->db->where('email', $email);
        }
        return $this->db->get($this->table)->row();
    }
    
    public function getById($id)
    {
        if($id != ''){
            $this->db->where('id', $id);
        }
        return $this->db->get($this->table)->row();
    }

    public function getData($user_id = '')
    {
        $this->db->select('id,name,title,position');
        if($user_id != ''){
            $this->db->where('id !=',$user_id);
        }
        $data = $this->db->get_where($this->table, 'level != 1')->result_array();
        return $data;
    }

    public function getDataDropdown()
    {
        $this->db->select('id,name,title,position');
        $this->db->where('level !=',1);
        $data = $this->db->get_where($this->table, 'level != 1')->result_array();
        return $data;
    }

    public function insertData($data)
    {
        return $this->db->insert($this->table,$data);
    }

    public function updateData($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update($this->table,$data);
    }

    public function deleteData($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }

    public function getAllUsers()
    {
        return $this->db->get_where($this->table,'level != 1')->result_array();
    }

}
