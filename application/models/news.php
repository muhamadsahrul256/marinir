<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class news extends CI_Model {
    public $typeSender = ['receiver' => 1, 'cc' => 2, 'bcc' => 3];

    public function getData($userId = '')
    {
        $this->db->select('*,tb_news.title as title_news,tb_news.id as news_id');
        if($userId != ''){
            $this->db->where('user_id',$userId);
        }
        $this->db->where('status','0');
        $this->db->join('tb_users','tb_news.user_id = tb_users.id');
        $this->db->order_by('tb_news.id',"DESC");
        return $this->db->get('tb_news')->result_array();
    }

    public function getDataIn($userId = '')
    {
        $this->db->select('*,tb_news.title as title_news,tb_news_in.id as news_id,tb_news_in.news_id as id_berita,CASE WHEN tb_news_in.type = 1 THEN "Penerima" WHEN tb_news_in.type = 2 THEN "Tembusan" WHEN tb_news_in.type = 3 THEN "Blind Carbon Copy(BCC)" END AS status_in_news ');
        if($userId != ''){
            $this->db->where('tb_news_in.user_id',$userId);
        }
        $this->db->where('tb_news_in.status','0');
        $this->db->join('tb_news','tb_news_in.news_id = tb_news.id','LEFT');
        $this->db->join('tb_users','tb_news.user_id = tb_users.id','LEFT');
        $this->db->order_by('tb_news.id',"DESC");
        return $this->db->get('tb_news_in')->result_array();
    }

    public function getNotifikasi($userId = '')
    {
        if($userId != ''){
            $this->db->where('tb_news_in.user_id',$userId);
        }
        $this->db->where('tb_news_in.is_read', 0);
        $this->db->where('tb_news_in.status', 0);
        return $this->db->count_all_results('tb_news_in');
    }

    public function insertData($data)
    {
        $this->db->insert('tb_news',$data);
        return $this->db->insert_id();
    }

    public function insertNewsIn($data)
    {
        return $this->db->insert('tb_news_in',$data);
    }

    public function deleteNews($newsId)
    {
        $data = array(
            'status' => 1,
            'deleted_date' => date('Y-m-d H:i:s')
        );
        $this->db->where('id',$newsId);
        return $this->db->update('tb_news',$data);
    }

    public function deleteNewsIn($newsId)
    {
        $this->db->where('id',$newsId);
        return $this->db->delete('tb_news_in');
    }

    public function getDataNews($id_berita)
    {
        $this->db->where('tb_news.id ',$id_berita);
        $this->db->where('tb_news.status','0');
        $this->db->order_by('tb_news.id',"DESC");
        return $this->db->get('tb_news')->row();
    }

    public function getOptionForm($id_berita,$type)
    {
        $this->db->select('tb_news_in.user_id as user_id');
        $this->db->where('news_id',$id_berita);
        $this->db->where('type',$type);
        return $this->db->get('tb_news_in')->result_array();
    }

    public function getComponentMessage($id = '')
    {
        $this->db->select('tb_users.title as user_title, tb_users.name as username, tb_users.position as position');
        $this->db->select('tb_news.title as news_title, tb_news.content as news_content');
        $this->db->select('tb_news.jenis as news_jenis, tb_news.nomor as news_nomor');
        $this->db->select('tb_news.derajat as news_derajat, tb_news.waktu_pengunjukan as news_waktu_pengunjukan');
        $this->db->select('tb_news.category as news_category');
        if($id != ''){
            $this->db->where('tb_news.id',$id);
        }
        $this->db->join('tb_users','tb_news.user_id = tb_users.id');

        $get = $this->db->get('tb_news');
        return $get->row();
    }

    public function getReceiver($id = '', $type = 0)
    {
        $this->db->select('tb_users.title as user_title, tb_users.name as username, tb_users.position as position');
        if($id != ''){
            $this->db->where('tb_news_in.news_id', $id);
        }

        $this->db->where('tb_news_in.type', $type);
        $this->db->join('tb_users','tb_news_in.user_id = tb_users.id');

        $get = $this->db->get('tb_news_in');
        return $get->result();
    }

}
