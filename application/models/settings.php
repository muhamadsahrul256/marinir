<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class settings extends CI_Model {

    public function getSetting()
    {
        return $this->db->get('tb_settings')->row_array();
    }

    public function UpdateSetting($data)
    {
        $data_setting = $this->db->get('tb_settings')->row();

        $this->db->where('id', $data_setting->id);
        $update = $this->db->update('tb_settings', $data);
        
        return $update;
    }

}