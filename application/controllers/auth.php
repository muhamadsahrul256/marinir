<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->has_userdata('email') && $this->uri->segment(1) != 'logout') {
			redirect('admin');
		}
		$this->load->model('users');
	}

	public function index()
	{
		$this->load->view('login');
		if($data = $this->input->post()) {
			$email = $data['email'];
			$password = $data['password'];
			$get_user = $this->users->getUsers($email);
			if(isset($get_user->email)) {
				// $password = password_hash($userpass, PASSWORD_BCRYPT, ['cost' => 10]);
				if (password_verify($password, $get_user->password)) {
					unset($get_user->password);
					$this->session->set_userdata((array) $get_user);
					redirect('admin');
				} else {
					$this->session->set_flashdata('alert', 'Salah password!');
					redirect('login');
				}
			}
			else {
				$this->session->set_flashdata('alert', 'Username tidak ada!');
				redirect('login');
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('./');
	}
}
