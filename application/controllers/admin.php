<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$uri = $this->uri;
		if(!$this->session->has_userdata('email') && ($uri->segment(1) != '')) {
			redirect('login');
		}
		$this->load->model('news');
		$this->load->model('settings');
		$this->load->model('users');
		$this->load->helper('download');

		$count_notifikasi = $this->news->getNotifikasi($this->session->userdata('id'));
		$this->session->set_userdata('notifikasi', $count_notifikasi);
	}

	public function index()
	{
		$data['setting'] = $this->settings->getSetting();
		$data['content'] = 'admin/dashboard';
		$this->load->view('admin/index', $data);
	}

	public function homepage()
	{
		$data['setting'] = $this->settings->getSetting();
		$this->load->view('homepage', $data);
	}

	public function table()
	{
		$data['data'] = [
			['col_1' => '1.1', 'col_2' => '1.2', 'col_3' => '1.3'],
			['col_1' => '2.1', 'col_2' => '2.2', 'col_3' => '2.3'],
			['col_1' => '3.1', 'col_2' => '3.2', 'col_3' => '3.3']
		];
		$data['content'] = 'admin/table';
		$this->load->view('admin/index', $data);
	}

	public function users()
	{
		$data['content'] = 'admin/form';
		$this->load->view('admin/index', $data);
	}

	public function berita_keluar()
	{
		$user_id = $this->session->userdata('id');
		$dataNews = $this->news->getData($user_id);
		$data['data'] = $dataNews;
		$data['content'] = 'admin/berita_keluar';
		$this->load->view('admin/index', $data);
	}

	public function berita_masuk()
	{
		$user_id = $this->session->userdata('id');
		$dataNews = $this->news->getDataIn($user_id);
		$data['data'] = $dataNews;
		$data['content'] = 'admin/berita_masuk';
		$this->load->view('admin/index', $data);
	}

	public function kirim_berita()
	{
		$data['data_user'] = $this->users->getData($this->session->userdata('id'));
		$data['content'] = 'admin/kirim_berita';
		$this->load->view('admin/index', $data);
	}

	public function send_berita()
	{
		$data = array(
			'title' 		=> $this->input->post('title'),
			'content' 		=> $this->input->post('content'),
			'file' 			=> $_FILES['file']['name'],
			'created_date' 	=> date('Y-m-d H:i:s'),
			'status'		=> '0',
			'user_id' 		=> $this->session->userdata('id'),
			'category' 		=> $this->input->post('kategori_berita'),
			'derajat'		=> $this->input->post('derajat'),
			'jenis'			=> $this->input->post('jenis'),
			'nomor'			=> $this->input->post('nomor'),
			'waktu_pengunjukan'	=> $this->input->post('waktu_pengunjukan'),
		);
		$news_id = $this->news->insertData($data);

		if (!file_exists('uploads/'.$news_id)) {
			mkdir('uploads/'.$news_id, 0777, true);
		}

		$config['upload_path']          = './uploads/'.$news_id;
		$config['allowed_types'] 		= '*';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('file'))
		{
			$error = array('error' => $this->upload->display_errors());
			
		}

		$receiver = $this->input->post('receiver');
		foreach($receiver as $val):
			$data = array(
				'news_id' => $news_id,
				'user_id' => $val,
				'status'  => '0',
				'is_read' => '0',
				'type'	  => '1',
			);
			$this->news->insertNewsIn($data);
		endforeach;

		$cc = $this->input->post('cc');
		if(!empty($cc)){
			foreach($cc as $val):
				$data = array(
					'news_id' => $news_id,
					'user_id' => $val,
					'status'  => '0',
					'is_read' => '0',
					'type'	  => '2',
				);
				$this->news->insertNewsIn($data);
			endforeach;
		}


		/*$bcc = $this->input->post('bcc');
		if(!empty($bcc)){
			foreach($bcc as $val):
				$data = array(
					'news_id' => $news_id,
					'user_id' => $val,
					'status'  => '0',
					'is_read' => '0',
					'type'	  => '3',
				);
				$this->news->insertNewsIn($data);
			endforeach;
		}*/
		
		redirect(base_url('admin/berita-keluar'));
	}

	public function save_user()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT, ['cost' => 10]),
			'nrp' => $this->input->post('nrp'),
			'title' => $this->input->post('title'),
			'position' => $this->input->post('position'),
			'level'	=> 2,
		);
		$this->users->insertData($data);
		redirect(base_url('admin/list-users'));
	}

	public function print_berita($id)
	{
		$this->load->library('PdfGenerator');
		$component_message = $this->news->getComponentMessage($id);
		$receiver = $this->news->getReceiver($id, $this->news->typeSender['receiver']);
		$cc = $this->news->getReceiver($id, $this->news->typeSender['cc']);

		$data['component_message'] = $component_message;
		$data['receiver'] = array_map(function($v) {
			return $v->position;
		}, $receiver);
		$data['cc'] = array_map(function($v) {
			return $v->position;
		}, $cc);

	    $html = $this->load->view('admin/print_berita', $data, true);
	    
	    $this->pdfgenerator->generate($html,'berita');
	}

	public function settings()
	{
		$dataSettings = $this->settings->getSetting();
		$data_icon = json_decode($dataSettings['icons']);
		$data['data'] = $dataSettings;
		$data['data_icons'] = $data_icon;
		$data['content'] = 'admin/settings';
		$this->load->view('admin/index', $data);
	}

	public function save_setting()
	{
		$welcome_text = $this->input->post('welcome_text');
		$icon_icon = $this->input->post('icon_icon');
		$icon_name = $this->input->post('icon_name');
		$icon_data = $this->input->post('icon_data');
		$file_hidden = $this->input->post('file_hidden');
		$data_icon = [];

		for ($i=1; $i <= 3; $i++) { 
			$data_icon[] = [
				'icon' => $icon_icon[$i],
				'name' => $icon_name[$i],
				'data' => $icon_data[$i],
				'file' => $file_hidden[$i],
			];
		}
		
		if (!empty($_FILES['icon_file1']['name']) ||
		!empty($_FILES['icon_file2']['name']) ||
		!empty($_FILES['icon_file3']['name'])) {
			$config['upload_path']          = './assets/images/icons';
			$config['allowed_types'] 		= '*';
	
			$this->load->library('upload', $config);
			for ($i=1; $i <= 3 ; $i++) {
				$min_1 = $i - 1;
				if(!empty($_FILES['icon_file'.$i]['name'])){
					if ( ! $this->upload->do_upload('icon_file'.$i)) {
						echo $this->upload->display_errors(); 
						exit;
					}
					else {
						$data_icon[$min_1]['file'] = "/images/icons/{$_FILES['icon_file'.$i]['name']}";
					}
				}
			}
		}

		if ($banner_file = $_FILES['banner']['name']) {
			$config['upload_path']          = './assets/images/';
			$config['allowed_types'] 		= '*';
	
			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload('banner'))
			{
				$error = array('error' => $this->upload->display_errors());
			}
			$banner = '/images/'.$banner_file;
		}


		$data_insert = array(
			'welcome_text' => $welcome_text,
			'icons'		   => json_encode($data_icon)
		);
		
		if (isset($banner)) {
			$data_insert['banner'] = $banner;
		}

		$this->settings->UpdateSetting($data_insert);
		redirect(base_url('admin/settings'));
	}

	public function list_users()
	{
		$dataUsers = $this->users->getAllUsers();
		$data['data'] = $dataUsers;
		$data['content'] = 'admin/list_users';
		$this->load->view('admin/index', $data);
	}

	public function download_file($url='')
	{
		force_download(base64_decode($url),NULL);
		echo "<script>window.close();</script>";
	}
	
	public function delete_berita_keluar($id_berita)
	{
		$this->news->deleteNews($id_berita);
		redirect(base_url('admin/berita-keluar'));
	}

	public function delete_berita_masuk($id_berita)
	{
		$this->news->deleteNewsIn($id_berita);
		redirect(base_url('admin/berita-masuk'));
	}

	public function update_user()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'nrp' => $this->input->post('nrp'),
			'title' => $this->input->post('title'),
			'position' => $this->input->post('position'),
		);
		if ($password = $this->input->post('password')) {
			$data['password'] = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);
		}
		$this->users->updateData($this->input->post('id'), $data);
		redirect(base_url('admin/list-users'));
	}

	public function delete_user($id=0)
	{
		$this->users->deleteData($id);
		redirect(base_url('admin/list-users'));
	}

	public function edit_user($id=0)
	{
		$dataUsers = $this->users->getById($id);
		$data['id'] = $id;
		$data['data'] = $dataUsers;
		$data['content'] = 'admin/edit_user';
		$this->load->view('admin/index', $data);
	}

	public function view_berita_keluar($id_berita)
	{
		$dataNews = $this->news->getDataNews($id_berita);
		// get for option receiver
		$dataReceiver = $this->news->getOptionForm($id_berita,1);
		// get for option CC
		$dataCC = $this->news->getOptionForm($id_berita,2);
		// get for option CC
		$dataBCC = $this->news->getOptionForm($id_berita,3);

		$data['data_user'] = $this->users->getDataDropdown($this->session->userdata('id'));
		$data['content'] = 'admin/view_berita';
		$data['news'] = $dataNews;
		$data['receiver'] = $dataReceiver;
		$data['cc']	= $dataCC;
		$data['bcc']	= $dataBCC;
		$this->load->view('admin/index', $data);
	}

	public function view_berita_masuk($id_berita)
	{
		$dataNews = $this->news->getDataNews($id_berita);
		// get for option receiver
		$dataReceiver = $this->news->getOptionForm($id_berita,1);
		// get for option CC
		$dataCC = $this->news->getOptionForm($id_berita,2);
		// get for option CC
		$dataBCC = $this->news->getOptionForm($id_berita,3);
		
		$data['data_user'] = $this->users->getDataDropdown($this->session->userdata('id'));
		$data['content'] = 'admin/view_berita_masuk';
		$data['news'] = $dataNews;
		$data['receiver'] = $dataReceiver;
		$data['cc']	= $dataCC;
		$data['bcc']	= $dataBCC;
		$this->load->view('admin/index', $data);
	}
}
