﻿# Host: localhost  (Version 5.5.5-10.4.11-MariaDB)
# Date: 2020-02-11 12:31:06
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "tb_news"
#

DROP TABLE IF EXISTS `tb_news`;
CREATE TABLE `tb_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `file` longtext DEFAULT NULL,
  `category` int(11) DEFAULT NULL COMMENT '1 = normal, 2 = secret',
  `jenis` varchar(255) DEFAULT NULL,
  `nomor` varchar(255) DEFAULT NULL,
  `derajat` varchar(255) DEFAULT NULL,
  `waktu_pengunjukan` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '0 = active, 1 = deleted',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

#
# Data for table "tb_news"
#

INSERT INTO `tb_news` VALUES (1,'test','test',NULL,1,NULL,NULL,NULL,NULL,1,1,'2019-01-12 11:05:00','2019-01-12 11:05:00','2020-01-25 17:03:32'),(2,'test','<p>Test Doang</p>','PageSettings.pdf',1,NULL,NULL,NULL,NULL,1,0,'2020-01-25 16:28:39',NULL,NULL),(3,'document ','<p>Beneran test doang</p>','Dashboard.pdf',2,NULL,NULL,NULL,NULL,1,0,'2020-01-25 16:30:32',NULL,NULL),(4,'test','<p>test</p>','PageSettings.pdf',1,NULL,NULL,NULL,NULL,1,0,'2020-01-26 05:37:04',NULL,NULL),(5,'test','<p>tess</p>','Timeline.xlsx',2,NULL,NULL,NULL,NULL,1,0,'2020-01-26 05:38:44',NULL,NULL),(6,'test','<p>tess</p>','Timeline.xlsx',2,NULL,NULL,NULL,NULL,1,0,'2020-01-26 05:40:29',NULL,NULL),(7,'test','<p>tess</p>','Timeline.xlsx',2,NULL,NULL,NULL,NULL,1,0,'2020-01-26 05:41:36',NULL,NULL),(8,'Test','<p>test</p>','',1,NULL,NULL,NULL,NULL,1,0,'2020-01-30 15:32:51',NULL,NULL),(9,'Test','<p>test</p>','',1,NULL,NULL,NULL,NULL,1,0,'2020-01-30 15:33:23',NULL,NULL),(10,'Pengumuman','KLASIFIKASI : Umum \r\nNo. 123\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet \r\nante et turpis blandit imperdiet. Donec porttitor vel libero sit amet \r\nsemper. Curabitur luctus leo et est rutrum posuere. Proin dictum aliquet\r\n arcu, non finibus dolor interdum nec. Donec ac nulla lectus. Integer \r\nconsectetur purus sagittis nisi sagittis pulvinar. Praesent quis odio \r\nfermentum, luctus diam eget, sagittis urna. Fusce interdum imperdiet \r\nipsum, nec porta quam blandit vel. Suspendisse molestie viverra diam at \r\nmaximus. Nulla ipsum tellus, rhoncus eget sodales vitae, dignissim id \r\nmagna. Nunc rutrum nec mi ac pulvinar. Maecenas mauris purus, sodales in\r\n lorem eget, pretium feugiat quam.','tahu.png',1,'132','123','213','2020-02-02',1,0,'2020-02-02 05:48:38',NULL,NULL);

#
# Structure for table "tb_news_in"
#

DROP TABLE IF EXISTS `tb_news_in`;
CREATE TABLE `tb_news_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1 = receiver, 2 = cc, 3 = bcc',
  `status` int(11) DEFAULT NULL COMMENT '0 = active, 1 = deleted',
  `is_read` int(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

#
# Data for table "tb_news_in"
#

INSERT INTO `tb_news_in` VALUES (3,2,2,1,0,0),(4,2,2,2,0,0),(5,2,4,2,0,0),(6,3,2,1,0,0),(7,3,2,2,0,0),(8,3,2,3,0,0),(9,4,2,1,0,0),(10,4,2,2,0,0),(11,4,2,3,0,0),(12,7,2,1,0,0),(13,7,2,2,0,0),(14,7,2,3,0,0),(15,9,2,1,0,0),(16,10,2,1,0,0),(17,10,4,2,0,0);

#
# Structure for table "tb_settings"
#

DROP TABLE IF EXISTS `tb_settings`;
CREATE TABLE `tb_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner` text DEFAULT NULL,
  `icons` text DEFAULT NULL,
  `welcome_text` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

#
# Data for table "tb_settings"
#

INSERT INTO `tb_settings` VALUES (11,'/images/backdrop.jpg','[{\"icon\":\"far fa-address-card\",\"name\":\"Personil\",\"data\":\"50\",\"file\":\"\\/images\\/icons\\/signature.png\"},{\"icon\":\"far fa-eye\",\"name\":\"Test1\",\"data\":\"13\",\"file\":\"\\/images\\/icons\\/square-linkedin-512.png\"},{\"icon\":\"far fa-copy\",\"name\":\"Test2\",\"data\":\"12\",\"file\":\"\\/images\\/icons\\/2x3.jpg\"}]','Latihan Korps Marinir');

#
# Structure for table "tb_users"
#

DROP TABLE IF EXISTS `tb_users`;
CREATE TABLE `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `NRP` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL COMMENT '1 = admin, 2 = user',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

#
# Data for table "tb_users"
#

INSERT INTO `tb_users` VALUES (1,'Muhamad Sahrul','muhamadsahrul256@gmail.com','Mjr','0082937478','Admin','$2y$10$COeCrhWAcH17KjUzXHsdSei1XA7qbJELSgJ/aylK96YkRNG8CRazq',1),(2,'Ahmad Ilham','aailham007@gmail.com','Gans','0012345678','Admin Biasa','$2y$10$COeCrhWAcH17KjUzXHsdSei1XA7qbJELSgJ/aylK96YkRNG8CRazq',2),(4,'Anton','test@test.test','Komandan','12343','Kasal','$2y$10$lcqss22mqqn6AB1SDcslC.1d7bHVJ.RpRP.GjMuV1gAtXu.XnMvWi',2);
